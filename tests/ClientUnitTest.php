<?php
namespace App\Tests\Entity;

use App\Entity\Client;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    public function testClientEntity()
    {
        // Create a Client entity
        $client = new Client();
        
        // Set some values for testing
        $client->setNom('John Doe');
        $client->setCin('12345678');
        
        // Assert that getters return the correct values
        $this->assertEquals('John Doe', $client->getNom());
        $this->assertEquals('12345678', $client->getCin());

        
    }
}

